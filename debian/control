Source: dune-grid
Section: libs
Priority: optional
Standards-Version: 4.7.0
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Ansgar <ansgar@debian.org>, Markus Blatt <markus@dr-blatt.de>
Vcs-Browser: https://salsa.debian.org/science-team/dune-grid
Vcs-Git: https://salsa.debian.org/science-team/dune-grid.git
Homepage: https://www.dune-project.org/
Build-Depends: debhelper-compat (= 13),
 cmake (>= 3.13), gfortran, mpi-default-bin, mpi-default-dev, pkgconf, python3,
 chrpath,
 libdune-common-dev (>= 2.10~),
 libdune-geometry-dev (>= 2.10~),
 libdune-uggrid-dev (>= 2.10~),
 libalberta-dev (>= 2.0.1-5),
 libgmp-dev,
 python3-vtk9
Build-Depends-Indep: doxygen, ghostscript, graphviz, imagemagick, texlive-latex-recommended, texlive-pictures
Rules-Requires-Root: no

Package: libdune-grid-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends},
 libdune-common-dev (>= 2.10~),
 libdune-geometry-dev (>= 2.10~),
 libdune-uggrid-dev (>= 2.10~),
 libalberta-dev (>= 2.0.1-5)
Suggests: libdune-grid-doc (= ${source:Version})
Provides: ${dune:shared-library}
Description: toolbox for solving PDEs -- grid interface (development files)
 DUNE, the Distributed and Unified Numerics Environment is a modular toolbox
 for solving partial differential equations (PDEs) with grid-based methods.
 It supports the easy implementation of methods like Finite Elements (FE),
 Finite Volumes (FV), and also Finite Differences (FD).
 .
 This package contains the development files for the grid interface.

Package: libdune-grid-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Built-Using: ${dune:doc:Built-Using}
Description: toolbox for solving PDEs -- grid interface (documentation)
 DUNE, the Distributed and Unified Numerics Environment is a modular toolbox
 for solving partial differential equations (PDEs) with grid-based methods.
 It supports the easy implementation of methods like Finite Elements (FE),
 Finite Volumes (FV), and also Finite Differences (FD).
 .
 This package contains the documentation for the grid interface.
